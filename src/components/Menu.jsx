import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { auth } from '../firebaseconfig'

const Menu = () => {
    const navigation = useNavigate()
    const [usuario, setUsuario] = useState(null)

    useEffect(() => {
        auth.onAuthStateChanged((user) => {
            if (user) {
                setUsuario(user.email)
                console.log(user.email)
            }
        })
    }, [])

    const LogOut = () => {
        auth.signOut()
        setUsuario(null)
        navigation('/')
    }


    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <ul className='navbar-nav mr-auto'>
                    <li className="nav-item">
                        <Link className="nav-link" to="/">Inicio</Link>
                    </li>
                    <li className="nav-item">
                        {
                            !usuario ?
                                (
                                    <Link className="nav-link" to="/login">Login</Link>
                                )
                                :
                                (
                                    <span></span>
                                )

                        }
                        
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/admin">Admin</Link>
                    </li>
                </ul>
                {
                    usuario ?
                        (
                            <button
                                className="btn btn-danger"
                                onClick={LogOut}
                            >Log out</button>
                        )
                        :
                        (
                            <span></span>
                        )

                }
            </nav>
        </div>
    )
}

export default Menu
