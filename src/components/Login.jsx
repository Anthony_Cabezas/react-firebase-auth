import { signInWithEmailAndPassword } from '@firebase/auth'
import React, {useState} from 'react'
import {auth, createUserWithEmailAndPassword} from '../firebaseconfig'
import { useNavigate } from "react-router-dom"

const Login = () => {
    const navigate = useNavigate()
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')
    const[msgerror, setMsgError] = useState(null)

    const RegistrarUsuario = (e) => {
        e.preventDefault()

    createUserWithEmailAndPassword(auth,email, password)
            .then( r => {
                navigate('/')
            })
            .catch (e => {
            if(e.code === 'auth/invalid-email'){
                setMsgError('Incorrect Email Format')
            }
            if(e.code === 'auth/weak-password'){
                setMsgError('Password must be 6 characters or longer')
            }
        })
        
    }
    
    const LoginUser = () => {
        signInWithEmailAndPassword(auth,email, password)
        .then((r)=> {
            navigate('/')
        })
        .catch((error)=> {
            if(error.code === 'auth/wrong-password'){
                setMsgError('Incorrect Password')
            }
        })
    }

    return (
        <div className='row mt-5'>
            <div className="col"></div>
            <div className="col">
                <form onSubmit={RegistrarUsuario} className='form-group'>
                    <input
                        onChange={(e)=> {setEmail(e.target.value)}}
                        className='form-control' 
                        placeholder='Email'
                        type="email" />
                    <input
                        onChange={(e)=> {setPassword(e.target.value)}}
                        className='form-control mt-4' 
                        placeholder='Password'
                        type="password" />
                    <input
                        className='btn btn-dark w-100 mt-4'
                        type="submit" 
                        value="Register User" />
                </form>
                <button 
                    className='btn btn-success w-100 mt-4'
                    onClick={LoginUser}
                >Login</button>
                {
                    msgerror ?
                    (
                        <div className='alert alert-danger w-100 mt-4'>
                            {msgerror}
                        </div>
                    )
                    :
                    (
                        <span></span>
                    )
                }
            </div>

            <div className="col"></div>
        </div>
    )
}

export default Login
