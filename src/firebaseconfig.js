import { initializeApp } from "firebase/app";
import {getAuth , createUserWithEmailAndPassword, signInWithEmailAndPassword} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDEoVJDRS8Jl_pumLRLCFcHlplL8jbb6NM",
    authDomain: "authentificar.firebaseapp.com",
    projectId: "authentificar",
    storageBucket: "authentificar.appspot.com",
    messagingSenderId: "527822011887",
    appId: "1:527822011887:web:b636fa12a099a22b4e4a1a",
    measurementId: "G-94B9FNK2TE"
  };


// Initialize Firebase
const app = initializeApp(firebaseConfig)
const auth = getAuth(app)

export {auth, createUserWithEmailAndPassword, signInWithEmailAndPassword}